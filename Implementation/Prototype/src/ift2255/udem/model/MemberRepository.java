package ift2255.udem.model;

import java.util.HashMap;

// TODO - use generics to factorize Member and Professional Repository into one

// MemberRepository keeps track of all members in a hash map
// the hash map key is: memberId
public class MemberRepository {
    private static MemberRepository _instance = null;

    // TODO - eventually replace with file... (but for prototype keep everything in memory)
    private HashMap<Integer, Member> memberList;

    private MemberRepository() {
        memberList = new HashMap<>();

        populate(); // hack to populate the database (for the prototype only)
    }

    public static MemberRepository getInstance() {
        if (_instance == null) {
            _instance = new MemberRepository();
        }
        return _instance;
    }

    // CRUD (Create, Read, Update and Delete) mocking implementations

    // CRUD's Create and Update
    public void add(Member member) {
        memberList.put(member.getId(), member);
    }

    // CRUD's Delete
    public void delete(int memberId) {
        memberList.remove(memberId);
    }


    // CRUD's Read
    public Member find(int memberId) {
        return memberList.get(memberId);
    }


    // a hack method to populate the database
    private void populate() {
        try {
            Member m = new Member();
            m.setName("Marie Laforce");
            m.setAddress("777 rue Dupire, Montréal, QC");
            m.setEmail("marie.laforce@wtf.com");
            m.setComment("Il y a de la joie!");
            add(m);

            m = new Member();
            m.setName("Julie Delatourfonduequipenche");
            m.setAddress("222 rue Boulimique, Montréal, QC");
            m.setEmail("julie.machin@whocare.com");
            m.setComment("Pousse, mais pas trop fort!");
            add(m);
        } catch (IdOutOfRangeException e) {
            // do nothing
        }
    }
}
