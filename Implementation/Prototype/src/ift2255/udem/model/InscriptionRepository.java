package ift2255.udem.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

// The InscriptionRepository keeps all inscription in a hash map
// the hash map key is: serviceId + memberId + serviceDate
public class InscriptionRepository {
    private static InscriptionRepository _instance = null;
    private static final String ID_FORMAT = "%07d%09d%s";

    // TODO - eventually replace with file... (but for prototype keep everything in memory)
    private HashMap<String, Inscription> inscriptionHashMap;

    private InscriptionRepository() {
        inscriptionHashMap = new HashMap<>();
    }

    public static InscriptionRepository getInstance() {
        if (_instance == null) {
            _instance = new InscriptionRepository();
        }
        return _instance;
    }

    // CRUD (Create, Read, Update and Delete) mocking implementations

    // CRUD's Create and Update
    public void add(Inscription inscription) {
        String inscriptionId = String.format(ID_FORMAT, inscription.getServiceId(), inscription.getMemberId(),
                inscription.getServiceDate());
        inscriptionHashMap.put(inscriptionId, inscription);
    }

    // CRUD's Delete
    public Inscription delete(int serviceId, int memberId, String serviceDate) {
        String inscriptionId = String.format(ID_FORMAT, serviceId, memberId, serviceDate);
        return inscriptionHashMap.remove(inscriptionId);
    }

    // CRUD's Read
    public Inscription find(int serviceId, int memberId, String serviceDate) {
        String inscriptionId = String.format(ID_FORMAT, serviceId, memberId, serviceDate);
        return inscriptionHashMap.get(inscriptionId);
    }

    // returns the list of inscriptions to a professional services
    public ArrayList<Inscription> findByProfessionalId(int professionalId, String date) {
        ArrayList<Inscription> result = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyy");
        LocalDate today = LocalDate.parse(date, formatter);
        for (Inscription inscription : inscriptionHashMap.values()) {
            if (inscription.getProfessionalId() == professionalId) {
                LocalDate theDate = LocalDate.parse(inscription.getServiceDate(), formatter);
                // Only add inscriptions from now on
                if (!theDate.isBefore(today)) result.add(inscription);
            }
        }
        return result;
    }

    // returns the list of inscription of a given member for a given date
    public ArrayList<Inscription> findByMemberIdAndDate(int memberId, String date) {
        ArrayList<Inscription> result = new ArrayList<>();
        for (Inscription inscription : inscriptionHashMap.values()) {
            if (inscription.getMemberId() == memberId && date.equals(inscription.getServiceDate()))
                result.add(inscription);
        }
        return result;
    }

    // returns all inscriptions as a collection
    public Collection<Inscription> all() {
        return inscriptionHashMap.values();
    }

}
