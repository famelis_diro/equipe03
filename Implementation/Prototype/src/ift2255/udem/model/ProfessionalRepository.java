package ift2255.udem.model;

import java.util.HashMap;

// TODO - use generics to factorize Member and Professional Repository into one

// ProfessionalRepository keeps track of all professional in a hash map
// the hash map key is: professionalId
public class ProfessionalRepository {
    private static ProfessionalRepository _instance = null;

    // TODO - eventually replace with file... (but for prototype keep everything in memory)
    private HashMap<Integer, Professional> professionalList;

    private ProfessionalRepository() {
        professionalList = new HashMap<>();

        populate(); // hack to populate the database (for the prototype only)
    }

    public static ProfessionalRepository getInstance() {
        if (_instance == null) {
            _instance = new ProfessionalRepository();
        }
        return _instance;
    }

    // CRUD (Create, Read, Update and Delete) mocking implementations

    // CRUD's Create and Update
    public void add(Professional professional) {
        professionalList.put(professional.getId(), professional);
    }

    // CRUD's Delete
    public void delete(int professionalId) {
        professionalList.remove(professionalId);
    }


    // CRUD's Read
    public Professional find(int professionalId) {
        return professionalList.get(professionalId);
    }

    // a hack method to populate the database
    private void populate() {
        try {
            Professional p = new Professional();
            p.setName("Pierre-Paul Jean");
            p.setAddress("45 rue St-Antoine, Montréal, QC");
            p.setEmail("pierre.paul.jean@nowhere.com");
            p.setComment("Un jour on verra bien...");
            add(p);

            p = new Professional();
            p.setName("Sophie Maheux");
            p.setAddress("677 rue Beaudry, Montréal, QC");
            p.setEmail("sophie.maheux@somewhere.com");
            p.setComment("A beau mentir qui vient de loin!");
            add(p);
        } catch (IdOutOfRangeException e) {
            // do nothing
        }
    }
}
