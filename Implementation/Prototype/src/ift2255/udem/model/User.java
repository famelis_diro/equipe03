package ift2255.udem.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

// Abstract class with implementation given by Member and Professional
// Provide basic information for both Members and Professionals
public abstract class User {

    protected static final int MAX_ID = 999999999;

    private int id;                           // (9 digits)
    private String creationDate;              // (DD-MM-yyyy HH:MM:SS)
    private String modificationDate;          // (DD-MM-yyyy HH:MM:SS)
    private Boolean suspended;                // true if suspended
    private Boolean deleted;                  // true if this member should be deleted
    private String name;                      // for the prototype, only use basic info
    private String address;                   // for the prototype, only use basic info
    private String email;                     // for the prototype, only use basic info
    private String comment;                   // (100 characters) (optional)

    // create a new user with empty attributes except for id and creation date
    public User() throws IdOutOfRangeException {
        this.creationDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
        this.modificationDate = creationDate;
        this.id = nextId();
        this.suspended = false;
        this.deleted = false;
        this.name = "";
        this.address = "";
        this.email = "";
        this.comment = "";
    }

    protected abstract int nextId() throws IdOutOfRangeException;


    public String getCreationDate() {
        return creationDate;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    private void setModificationDate() {
        this.modificationDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
    }

    public int getId() {
        return id;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
        setModificationDate();
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
        setModificationDate();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        setModificationDate();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        setModificationDate();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        setModificationDate();
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
        setModificationDate();
    }

    @Override
    public String toString() {
        return id + "\n" +
                creationDate + "\n" +
                modificationDate + "\n" +
                suspended + "\n" +
                deleted + "\n" +
                name + "\n" +
                address + "\n" +
                email + "\n" +
                comment + "\n";
    }

    // Default details information
    public String details() {
        return "Numéro : " + id + "\n" +
                "Date d'inscription : " + creationDate + "\n" +
                "Date de modification : " + modificationDate + "\n" +
                "Suspendu : " + (suspended ? "oui" : "non") + "\n" +
                "Supprimer : " + (deleted ? "oui" : "non") + "\n" +
                "Nom :" + name + "\n" +
                "Adresse :" + address + "\n" +
                "Courriel :" + email + "\n" +
                "Commentaire : " + comment + "\n";
    }
}
