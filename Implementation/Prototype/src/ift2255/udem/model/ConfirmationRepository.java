package ift2255.udem.model;

import java.util.HashMap;

// The ConfirmationRepository hold the list of confirmations in a hash map
// serviceId + memberId + the first part of creation date is used as a key to uniquely identify a confirmation

public class ConfirmationRepository {
    private static ConfirmationRepository _instance = null;
    private static final String ID_FORMAT = "%07d%09d%s";

    // TODO - eventually replace with file... (but for prototype keep everything in memory)

    // TODO - This table has no proper unique key, to be consistent with other tables, the primary key should
    // TODO - be constituted of the serviceId + memberId + serviceDate (dd-MM-yyyy). Hence we NEED the field serviceDate
    // TODO - But for now, let's use the serviceId + memberId + first part of creationDate as a primary key
    // TODO - (it's the same but not really).
    private HashMap<String, Confirmation> confirmationHashMap;

    private ConfirmationRepository() {
        confirmationHashMap = new HashMap<>();
    }

    public static ConfirmationRepository getInstance() {
        if (_instance == null) {
            _instance = new ConfirmationRepository();
        }
        return _instance;
    }

    // CRUD (Create, Read, Update and Delete) mocking implementations

    // CRUD's Create and Update
    public void add(Confirmation confirmation) {
        String confirmationId = String.format(ID_FORMAT, confirmation.getServiceId(), confirmation.getMemberId(),
                confirmation.getCreationDate().substring(0,10));
        confirmationHashMap.put(confirmationId, confirmation);
    }

    // CRUD's Delete
    public Confirmation delete(int serviceId, int memberId, String serviceDate) {
        String inscriptionId = String.format(ID_FORMAT, serviceId, memberId, serviceDate);
        return confirmationHashMap.remove(inscriptionId);
    }

    // CRUD's Read
    public Confirmation find(int serviceId, int memberId, String serviceDate) {
        String inscriptionId = String.format(ID_FORMAT, serviceId, memberId, serviceDate);
        return confirmationHashMap.get(inscriptionId);
    }

}
