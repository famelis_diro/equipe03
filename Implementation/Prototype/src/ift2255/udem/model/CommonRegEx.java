package ift2255.udem.model;

import java.util.regex.Pattern;

// Class holding common static regex patterns used for automatic validation
public class CommonRegEx {
    public static final Pattern VALID_NAME =
            Pattern.compile("^[\\p{L} .'-]+$", Pattern.CASE_INSENSITIVE);

    public static final Pattern VALID_ADDRESS =
            Pattern.compile("^\\d{1,5}[ ][A-Za-z0-9., -]+$", Pattern.CASE_INSENSITIVE);

    public static final Pattern VALID_EMAIL =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static final Pattern VALID_COMMENT =
            Pattern.compile("^.{0,100}$", Pattern.CASE_INSENSITIVE);

    public static final Pattern VALID_DATE =
            Pattern.compile("^([0-2][1-9]|[1-3]0|31])-(0[1-9]|1[0-2])-2\\d\\d\\d$");

    public static final Pattern VALID_TIME =
            Pattern.compile("^[0-2]\\d:[0-5]\\d$");

    public static final Pattern VALID_PASSWD =
            Pattern.compile("^(.){4,10}$");
}
