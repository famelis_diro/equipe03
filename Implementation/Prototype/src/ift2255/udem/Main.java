package ift2255.udem;

import ift2255.udem.model.*;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// For the prototype, the Main class is used as the View and Controller of the MVC pattern
public class Main {
    // TODO - The following are NOT IMPLEMENTED in the protoype
    // TODO -   * Implement a task for "Procédure comptable"
    // TODO -   * Implement a task to manage requests from RnB (e.g. to suspend a member)
    // TODO -   * email report: The Summary Report is stored locally and not sent to the manager

    // TODO - BUG[Glitch]: in some cases, the scanner seems to assume there is still something to read
    // TODO   it's only a glitch since it just prints twice in a row the same question (to be fixed later)

    // TODO - BUG[Strange]: adding a member to the first service, make this service disappear from the printed list???
    // TODO   yet, the service is still in the serviceRepository since we can add more member to it (WTFrak???)

    // TODO - BUG[Conceptual]: The list of registered member is stored by service. This is wrong. They should be
    // TODO   stored by "séance"... But this entuty has not been created for the prototype

    // TODO - JavaDoc: since this is a prototype, bound to change a lot, we haven't done the JavaDoc, only short comments

    // TODO - Assumption : professionals are paid 20$/séance since no rule was provided in the problem

    private static MemberRepository memberRepository = MemberRepository.getInstance();
    private static ProfessionalRepository professionalRepository = ProfessionalRepository.getInstance();
    private static ServiceRepository serviceRepository = ServiceRepository.getInstance();
    private static InscriptionRepository inscriptionRepository = InscriptionRepository.getInstance();
    private static ConfirmationRepository confirmationRepository = ConfirmationRepository.getInstance();

    public static void main(String[] args) {
        String cmlMenu = "\nSélectionnez une des options suivantes :\n" +
                "\t 0) Quitter l'application\n" +
                "\t 1) Gérer un membre\n" +
                "\t 2) Valider un membre\n" +
                "\t 3) Inscription à une séance\n" +
                "\t 4) Confirmer une inscription\n" +
                "\t 5) Consulter le répertoire des services\n" +
                "\t 6) Gérer un professionnel\n" +
                "\t 7) Valider un professionnel\n" +
                "\t 8) Gérer un service\n" +
                "\t 9) Consulter les inscriptions aux séances\n" +
                "\t10) Générer un rapport de synthèse\n" +
                "\nEntrez le chiffre correspondant (de 0 à 10)\n";
        String prompt = "#GYM:> ";
        Scanner scanner = new Scanner(System.in);
        int menuOption = 100;

        System.out.println("\tBienvenue dans l'application du Centre de Données #GYM!");

        do {
            System.out.println(cmlMenu);
            System.out.print(prompt);
            if (scanner.hasNextInt()) {
                menuOption = scanner.nextInt();
            } else {
                scanner.next();  // silently ignore anything else than an int
                continue;
            }
            if (menuOption < 0) menuOption = 100;
            switch (menuOption) {
                case 1: // Gérer un membre
                    gererUnMembre(scanner);
                    break;
                case 2: // Valider un membre
                    validerUnMembre(scanner);
                    break;
                case 3: // Inscription à une séance
                    inscriptionSeance(scanner);
                    break;
                case 4: // Confirmer une inscription
                    confirmerInscription(scanner);
                    break;
                case 5: // Consulter le répertoire des services
                    afficherRepertoireDesServices();
                    break;
                case 6: // Gérer un professionnel
                    gererUnProfessionnel(scanner);
                    break;
                case 7: // Valider un professionnel
                    validerUnProfessionnel(scanner);
                    break;
                case 8: // Gérer un service
                    gererUnService(scanner);
                    break;
                case 9: // Consulter les inscriptions aux séances
                    consulterInscriptions(scanner);
                    break;
                case 10: // Générer un rapport de synthèse
                    genererRapportDeSynthese(scanner);
                    break;
                default: // do nothing
            }
        } while (menuOption > 0);

        scanner.close();
    }

    // this method is only called by Générer un Rapport de Synthèse
    // it makes sure the user as the right privileges and call the method that does the real job
    private static void genererRapportDeSynthese(Scanner scanner) {
        System.out.println("\n\t*** Générer un rapport de synthèse ***\n");

        boolean notAuthorized = true;
        int count = 1;
        do {
            System.out.println("\n\tEntez le mot de passe (essaie " + count + "/3) : ");
            String password = readField(scanner,"Mot de passe", CommonRegEx.VALID_PASSWD);
            if (password.equals("#GYM")) {
                notAuthorized = false;
                count = 4;
            } else {
                System.out.println("DEBUG : mot de passe = "+ password);
                count++;
            }
        } while (count < 4);

        if (notAuthorized) {
            System.out.println("\n\tErreur - Vous n'êtes pas authorisé à utiliser cette fonctionalité.");
        } else {
            rapportDeSynthese(true);
        }
    }

    // Generates le "Rapport de Synthèse" and write it to a file (email not supported yet)
    private static void rapportDeSynthese(boolean upToNow) {
        LocalDate now = LocalDate.now();
        LocalDate startDate, endDate;
        if (upToNow) {
            startDate = now.minusDays((now.getDayOfWeek().getValue() + 1) % 6);
            endDate = now.plusDays(1);
        } else { // day is friday, time is 24:00
            now = now.minusDays(1); // in case there was a delay and was executed a few seconds after midnight
            startDate = now.minusDays((now.getDayOfWeek().getValue() + 1) % 6);
            endDate = startDate.plusDays(7);
        }

        ArrayList<String> dateIncluded = new ArrayList<>();
        for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
            dateIncluded.add(date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        }

        // TODO - not the proper way to do, but it will work this way
        // TODO - proper way would involve adding a "seance" entity
        // let's first accumulate services with inscription and keep count of their occurances
        HashMap<Integer, Integer> serviceCount = new HashMap<>();
        for (Inscription inscription : inscriptionRepository.all()) {
            for (String date : dateIncluded) {
                if (inscription.getServiceDate().equals(date)) {
                    Integer count = serviceCount.get(inscription.getServiceId());
                    if (count == null) {
                        count = 1;
                    } else {
                        count = count + 1;
                    }
                    serviceCount.put(inscription.getServiceId(), count);
                    break;
                }
            }
        }

        // now we can just sum the total number of services provided by a professional that week
        try {
            FileWriter fw = new FileWriter("RapportDeSynthese.txt");
            BufferedWriter writer = new BufferedWriter(fw);
            for (HashMap.Entry<Integer, Integer> entry : serviceCount.entrySet()) {
                int serviceId = entry.getKey();
                // TODO - must check if service exists & professional exist in their respective repository
                int professionalId = serviceRepository.find(serviceId).getProfessionalId();
                String professionalName = professionalRepository.find(professionalId).getName();
                int nbSeances = entry.getValue();
                int frais = nbSeances * 20; // let's give 20$ "par séance" since there is no rule
                writer.write(professionalName + ", " + professionalId + ", " + nbSeances + ", " + frais + "\n");
            }

            System.out.println("\n\tRapport de synthèse créé et envoyé au gérant");
            // TODO - Yeah, right... need to send repport to the manager
            writer.close();
        } catch (IOException ex) {
            System.out.println("\n\tErreur - erreur lors de la génération du rapport de synthèse\n");
        }

    }

    // Allow a professional to see the list of "séances" that have registered member(s)
    private static void consulterInscriptions(Scanner scanner) {
        System.out.println("\n\t*** Consulter les Inscriptions ***\n");

        int professionalId = readIntField(scanner, "Numéro du professionnel", 1, Professional.getLastId() - 1);
        Professional professional = professionalRepository.find(professionalId);

        if (professional == null) {
            System.out.println("\n\tErreur - Numéro de professionnel invalide : " + professionalId);
        } else {
            String today = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
            ArrayList<Inscription> inscriptions = inscriptionRepository.findByProfessionalId(professionalId, today);

            if (inscriptions.isEmpty()) {
                System.out.println("\n\tErreur - Aucune inscription trouvée pour Numéro de professionnel "
                        + professionalId + " à partir de " + today);
            } else {
                System.out.println("\n\tInscriptions aux services du professionnel " + professionalId);
                for (Inscription inscription : inscriptions) {
                    System.out.println(inscription.detailsShort());
                }
            }
        }
    }

    // Allow a member to confirm registration to a service and generates a record (stored in memory for prototype)
    private static void confirmerInscription(Scanner scanner) {
        System.out.println("\n\t*** Confirmation d'une Inscription ***\n");

        int memberId = readIntField(scanner, "Numéro du membre", 1, Member.getLastId() - 1);
        Member member = memberRepository.find(memberId);
        if (member == null) {
            System.out.println("\n\tErreur - Numéro de membre invalide : " + memberId);
        } else {
            String today = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
            ArrayList<Inscription> inscriptions = inscriptionRepository.findByMemberIdAndDate(memberId, today);
            if (inscriptions.isEmpty()) {
                System.out.println("\n\tErreur - Aucune inscription trouvée pour Numéro de membre "
                        + memberId + " en date de " + today);
            } else {
                Inscription theInscription = inscriptions.get(0);
                if (inscriptions.size() > 1) {
                    System.out.println("\nSélectionner l'index précédent l'une des dates suivantes :");
                    for (int i=0; i<inscriptions.size()-1; i++) {
                        System.out.print("[" + i + "]" + inscriptions.get(i).getServiceDate() + ", ");
                    }
                    int last = inscriptions.size()-1;
                    System.out.println("[" + last + "]" + inscriptions.get(last).getServiceDate() + "\n");

                    int selectedDate = readIntField(scanner, "Index de la séance", 0,
                            inscriptions.size() - 1);
                    theInscription = inscriptions.get(selectedDate);
                }
                System.out.println("\n\tInscription retenue : " + theInscription.detailsShort());

                Confirmation confirmation= new Confirmation();
                confirmation.setMemberId(memberId);
                confirmation.setProfessionalId(theInscription.getProfessionalId());
                confirmation.setServiceId(theInscription.getServiceId());

                String comment = readField(scanner, "Commentaire", CommonRegEx.VALID_COMMENT);
                if (!comment.isEmpty()) confirmation.setComment(comment);

                confirmationRepository.add(confirmation);
                System.out.println("\n\tConfirmation à la séance Validée!");
            }
        }
    }

    // Allow a member to register to a service and generates a record (stored in memory for prototype)
    private static void inscriptionSeance(Scanner scanner) {
        System.out.println("\n\t*** Inscription à une Séance ***\n");

        Inscription inscription = consulterRepertoireDesServices(scanner);

        int memberId = readIntField(scanner, "Numéro du membre", 1, Member.getLastId() - 1);
        Member member = memberRepository.find(memberId);
        if (member == null) {
            System.out.println("\n\tErreur - Numéro de membre invalide : " + memberId);
        } else {
            inscription.setMemberId(memberId);
            String comment = readField(scanner, "Commentaire", CommonRegEx.VALID_COMMENT);
            if (!comment.isEmpty()) inscription.setComment(comment);

            if (confirm(scanner, "Confirmez l'inscription au service")) {
                if (inscriptionRepository.find(inscription.getServiceId(), memberId, inscription.getServiceDate()) != null) {
                    // member is already signed up for this service
                    System.out.println("\n\tErreur - Le membre est déjà inscrit à cette séance");
                } else {
                    inscriptionRepository.add(inscription);
                    Service service = serviceRepository.find(inscription.getServiceId());
                    service.add(member);
                    serviceRepository.add(service);
                    System.out.println("\n\tInscription confirmée : " + inscription.detailsShort());
                    System.out.println("\n\tMontant à payer pour la séance : "
                            + serviceRepository.find(inscription.getServiceId()).getCost() + "$");
                }
            }

        }
    }


    // Print the content of the service repository
    private static void afficherRepertoireDesServices() {
        System.out.println("\n\t*** Répertoire des Services ***\n");
        System.out.println(Service.detailsShortHeader());
        for (Service servItem : serviceRepository.all()) {
            // TODO - we should paginate, but won't do it for the prototype
            // only show services that haven't been deleted, are not full and have an end-date in the future
            if (!servItem.getDeleted() && !servItem.isFull() && servItem.isInTheFuture())
                System.out.println(servItem.detailsShort());
        }
    }

    // Print the content of the service repository and let the user select a service, return the details in Inscription
    private static Inscription consulterRepertoireDesServices(Scanner scanner) {
        if (serviceRepository.isEmpty()) {
            System.out.println("\n\tErreur - Le Répetoire des Services ne contient aucun service!");
            return null;
        }

        Service service = null;
        do {
            afficherRepertoireDesServices();
            System.out.println("\n\tSélection de la séance...\n");
            int serviceId = readIntField(scanner, "Code de service", 1, Service.getLastId());
            service = serviceRepository.find(serviceId);
            if (service == null) {
                System.out.println("\n\tErreur - Code de service invalide : " + serviceId);
            }
        } while (service == null);

        ArrayList<String> possibleDates = service.possibleDates();
        if (possibleDates.size() == 0) {
            System.out.println("\n\tErreur - Aucune séance disponible");
            return null;
        }

        System.out.println("\nSélectionner l'index précédent l'une des dates suivantes :");
        for (int i=0; i<possibleDates.size()-1; i++) {
            System.out.print("[" + i + "]" + possibleDates.get(i) + ", ");
        }
        int last = possibleDates.size()-1;
        System.out.println("[" + last + "]" + possibleDates.get(last) + "\n");

        int selectedDate = readIntField(scanner, "Index de la séance", 0, possibleDates.size() - 1);

        Inscription inscription = new Inscription();
        inscription.setProfessionalId(service.getProfessionalId());
        inscription.setServiceId(service.getId());
        inscription.setServiceDate(possibleDates.get(selectedDate));

        return inscription;
    }

    // Check if a member is still valid
    private static void validerUnMembre(Scanner scanner) {
        System.out.println("\n\t*** Validation d'un Membre ***\n");

        String errMsg1 = "\nErreur - Numéro de membre invalide.";
        String errMsg2 = "\nErreur - Membre suspendu.";
        System.out.print("Veuillez entrer le numéro du membre : ");
        int memberId = -1;
        Member theMember = null;
        if (scanner.hasNextInt()) {
            memberId = scanner.nextInt();
            if (!Member.idIsValid(memberId)) {
                System.out.println(errMsg1); // invalid member id
            } else {
                theMember = memberRepository.find(memberId);
                if (theMember == null || theMember.getDeleted()) {
                    System.out.println(errMsg1); // unknown member id or deleted member
                } else if (theMember.getSuspended()) {
                    System.out.println(errMsg2);
                } else {
                    System.out.println("\n\t*** Membre valide ***\n");
                }
            }
        } else { // this is not a member id
            scanner.nextLine();
            scanner.nextLine();
            System.out.println(errMsg1);
        }
    }

    // check if a professional is still valid
    private static void validerUnProfessionnel(Scanner scanner) {
        // TODO - this is the same as validerUnMembre... must factorize!!!
        System.out.println("\n\t*** Validation d'un Professionnel ***\n");

        String errMsg1 = "\nErreur - Numéro de professionnel invalide.";
        String errMsg2 = "\nErreur - Professionnel suspendu.";
        System.out.print("Veuillez entrer le numéro du professionnel : ");
        int id = -1;
        Professional theProfessional = null;
        if (scanner.hasNextInt()) {
            id = scanner.nextInt();
            if (!Professional.idIsValid(id)) {
                System.out.println(errMsg1); // invalid member id
            } else {
                theProfessional = professionalRepository.find(id);
                if (theProfessional == null || theProfessional.getDeleted()) {
                    System.out.println(errMsg1); // unknown professional id or deleted professional
                } else if (theProfessional.getSuspended()) {
                    System.out.println(errMsg2);
                } else {
                    System.out.println("\n\t*** Professionnel valide ***\n");
                }
            }
        } else { // this is not a professional id
            scanner.nextLine();
            scanner.nextLine();
            System.out.println(errMsg1);
        }
    }

    // Allow to add/update/delete a member (changes are kept in memory for prototype)
    private static void gererUnMembre(Scanner scanner) {
        System.out.println("\n\t*** Gérer un Membre ***\n");

        String errMsg1 = "\nErreur - le numéro du membre est un nombre à 9 chiffres.";
        String errMsg2 = "\nErreur - impossible de créer un nouveau membre, les numéros de membres sont épuisés.";
        String errMsg3 = "\nErreur - numéro de membre invalide.";
        Member theMember = null;
        Boolean newMember = false;

        // Member retrieval or creation of a new one
        System.out.print("Veuillez entrer le numéro du membre (ou 0 pour un nouveau membre) : ");
        int memberId = -1;
        if (scanner.hasNextInt()) {
            memberId = scanner.nextInt();
            if (memberId == 0) {
                try {
                    theMember = new Member();
                    System.out.println("\tNuméro attribué au nouveau membre : " + theMember.getId());
                    newMember = true;
                } catch (IdOutOfRangeException idOutOfRange) {
                    System.out.println(errMsg2); // cannot create new member
                    theMember = null;
                }
            } else if (!Member.idIsValid(memberId)) {
                System.out.println(errMsg3); // invalid member id
            } else {
                theMember = memberRepository.find(memberId);
                if (theMember == null) {
                    System.out.println(errMsg3); // unknown member id
                }
            }
        } else { // this is not a member id
            System.out.println(errMsg1);
        }

        // Delete member
        Boolean deleteMember = false;
        scanner.nextLine();
        if (theMember != null && !newMember) {
            deleteMember = confirm(scanner, "Désirez-vous supprimer ce membre");
            if (deleteMember) deleteMember = confirm(scanner, "Comfirmez la suppression");

            if (deleteMember) {
                theMember.setDeleted(true);
                memberRepository.add(theMember); // Update the member status
                System.out.println("\n\nLe membre suivant a été supprimé : \n" + theMember.details());
                // TODO - must remove member from future services and generate report
            } else System.out.println("\tLe membre " + memberId + " ne sera pas supprimé!");
        }

        // Add new member
        if (newMember) {
            // TODO - this is not optimal, but it will do for the prototype... (needs generalization)
            System.out.println("\t*** Ajout d'un nouveau membre ***");

            String name = readField(scanner, "Nom du membre", CommonRegEx.VALID_NAME);
            theMember.setName(name);

            String address = readField(scanner, "Adresse du membre", CommonRegEx.VALID_ADDRESS);
            theMember.setAddress(address);

            String email = readField(scanner, "Adresse courriel du membre", CommonRegEx.VALID_EMAIL);
            theMember.setEmail(email);

            String comment = readField(scanner, "Comentaire", CommonRegEx.VALID_COMMENT);
            theMember.setComment(comment);

            memberRepository.add(theMember);   // Create member in the repository
            System.out.println("\n\nLe membre suivant a été créé : \n" + theMember.details());
        }

        // Update existing member
        else if (theMember != null && !deleteMember) {
            // TODO - this is not optimal, but it will do for the prototype... (needs generalization)
            System.out.println("\t*** Modification d'un membre existant ***");

            String name = updateField(scanner, "Nom du membre", theMember.getName(),
                    CommonRegEx.VALID_NAME);
            if (!name.isEmpty()) theMember.setName(name);

            String address = updateField(scanner, "Adresse du membre", theMember.getAddress(),
                    CommonRegEx.VALID_ADDRESS);
            if (!address.isEmpty()) theMember.setAddress(address);

            String email = updateField(scanner, "Adresse courriel du membre", theMember.getEmail(),
                    CommonRegEx.VALID_EMAIL);
            if (!email.isEmpty()) theMember.setEmail(email);

            String comment = updateField(scanner, "Comentaire", theMember.getComment(),
                    CommonRegEx.VALID_COMMENT);
            if (!comment.isEmpty()) theMember.setComment(comment);

            memberRepository.add(theMember);   // Update member in the repository
            System.out.println("\n\nLe membre suivant a été modifié : \n" + theMember.details());
        }

    }

    // Allow to add/update/delete a professional (changes are kept in memory for prototype)
    private static void gererUnProfessionnel(Scanner scanner) {
        // TODO - This is the same as gererUnMembre, must factorize!!!
        System.out.println("\n\t*** Gérer un Professionnel ***\n");

        String errMsg1 = "\nErreur - le numéro du professionnel est un nombre à 9 chiffres.";
        String errMsg2 = "\nErreur - impossible de créer un nouveau professionnel, les numéros de professionnels sont épuisés.";
        String errMsg3 = "\nErreur - numéro de professionnel invalide.";
        Professional theProfessional = null;
        Boolean newProfessional = false;

        // Member retrieval or creation of a new one
        System.out.print("Veuillez entrer le numéro du professionnel (ou 0 pour un nouveau professionnel) : ");
        int professionalId = -1;
        if (scanner.hasNextInt()) {
            professionalId = scanner.nextInt();
            if (professionalId == 0) {
                try {
                    theProfessional = new Professional();
                    System.out.println("\tNuméro attribué au nouveau professionel : " + theProfessional.getId());
                    newProfessional = true;
                } catch (IdOutOfRangeException idOutOfRange) {
                    System.out.println(errMsg2); // cannot create new member
                    theProfessional = null;
                }
            } else if (!Professional.idIsValid(professionalId)) {
                System.out.println(errMsg3); // invalid professional id
            } else {
                theProfessional = professionalRepository.find(professionalId);
                if (theProfessional == null) {
                    System.out.println(errMsg3); // unknown professional id
                }
            }
        } else { // this is not a member id
            System.out.println(errMsg1);
        }

        // Delete member
        Boolean deleteProfessional = false;
        scanner.nextLine();
        if (theProfessional != null && !newProfessional) {
            deleteProfessional = confirm(scanner, "Désirez-vous supprimer ce professionnel");
            if (deleteProfessional) deleteProfessional = confirm(scanner, "Comfirmez la suppression");

            if (deleteProfessional) {
                theProfessional.setDeleted(true);
                professionalRepository.add(theProfessional); // Update the professional status
                System.out.println("\n\nLe professionnel suivant a été supprimé : \n" + theProfessional.details());
                // TODO - must remove member from future services and generate report
            } else System.out.println("\tLe professionnel " + professionalId + " ne sera pas supprimé!");
        }

        // Add new member
        if (newProfessional) {
            // TODO - this is not optimal, but it will do for the prototype... (needs generalization)
            System.out.println("\t*** Ajout d'un nouveau professionnel ***");

            String name = readField(scanner, "Nom du professionnel", CommonRegEx.VALID_NAME);
            theProfessional.setName(name);

            String address = readField(scanner, "Adresse du professionnel", CommonRegEx.VALID_ADDRESS);
            theProfessional.setAddress(address);

            String email = readField(scanner, "Adresse courriel du professionnel", CommonRegEx.VALID_EMAIL);
            theProfessional.setEmail(email);

            String comment = readField(scanner, "Comentaire", CommonRegEx.VALID_COMMENT);
            theProfessional.setComment(comment);

            professionalRepository.add(theProfessional);   // Create member in the repository
            System.out.println("\n\nLe professionnel suivant a été créé : \n" + theProfessional.details());
        }

        // Update existing member
        else if (theProfessional != null && !deleteProfessional) {
            // TODO - this is not optimal, but it will do for the prototype... (needs generalization)
            System.out.println("\t*** Modification d'un professionnel existant ***");

            String name = updateField(scanner, "Nom du professionnel", theProfessional.getName(),
                    CommonRegEx.VALID_NAME);
            if (!name.isEmpty()) theProfessional.setName(name);

            String address = updateField(scanner, "Adresse du professionnel", theProfessional.getAddress(),
                    CommonRegEx.VALID_ADDRESS);
            if (!address.isEmpty()) theProfessional.setAddress(address);

            String email = updateField(scanner, "Adresse courriel du professionnel", theProfessional.getEmail(),
                    CommonRegEx.VALID_EMAIL);
            if (!email.isEmpty()) theProfessional.setEmail(email);

            String comment = updateField(scanner, "Comentaire", theProfessional.getComment(),
                    CommonRegEx.VALID_COMMENT);
            if (!comment.isEmpty()) theProfessional.setComment(comment);

            professionalRepository.add(theProfessional);   // Update professional in the repository
            System.out.println("\n\nLe professionnel suivant a été modifié : \n" + theProfessional.details());
        }

    }

    // Allow to add/update/delete a service (changes are kept in memory for prototype)
    private static void gererUnService(Scanner scanner) {
        // TODO - This is the same as gererUnMembre, must factorize!!!
        System.out.println("\n\t*** Gérer un Service ***\n");

        String errMsg1 = "\nErreur - le code de service est un nombre à 7 chiffres.";
        String errMsg2 = "\nErreur - impossible de créer un nouveau service, les codes de service sont épuisés.";
        String errMsg3 = "\nErreur - code de service invalide.";
        Service theService = null;
        Boolean newService = false;

        // Member retrieval or creation of a new one
        System.out.print("Veuillez entrer le code de service (ou 0 pour un nouveau service) : ");
        int serviceId = -1;
        if (scanner.hasNextInt()) {
            serviceId = scanner.nextInt();
            if (serviceId == 0) {
                try {
                    theService = new Service();
                    System.out.println("\tCode attribué au nouveau service : " + theService.getId());
                    newService = true;
                } catch (IdOutOfRangeException idOutOfRange) {
                    System.out.println(errMsg2); // cannot create new service
                    theService = null;
                }
            } else if (!Service.idIsValid(serviceId)) {
                System.out.println(errMsg3); // invalid service id
            } else {
                theService = serviceRepository.find(serviceId);
                if (theService == null) {
                    System.out.println(errMsg3); // unknown service id
                }
            }
        } else { // this is not a service id
            System.out.println(errMsg1);
        }

        // Delete service
        Boolean deleteService = false;
        scanner.nextLine();
        if (theService != null && !newService) {
            deleteService = confirm(scanner, "Désirez-vous supprimer ce service");
            if (deleteService) deleteService = confirm(scanner, "Comfirmez la suppression");

            if (deleteService) {
                theService.setDeleted(true);
                serviceRepository.add(theService); // Update the service status
                System.out.println("\n\nLe service suivant a été supprimé : \n" + theService.details());
                // TODO - must inform members and generate report
            } else System.out.println("\tLe service " + serviceId + " ne sera pas supprimé!");
        }

        // Add new service
        if (newService) {
            // TODO - this is not optimal, but it will do for the prototype... (needs generalization)
            System.out.println("\t*** Ajout d'un nouveau service ***");

            String name = readField(scanner, "Nom du service", CommonRegEx.VALID_NAME);
            theService.setName(name);

            boolean toEarly;
            String today = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
            String startDate;
            do {
                startDate = readField(scanner, "Date de début (JJ-MM-AAAA)", CommonRegEx.VALID_DATE);
                toEarly = isBefore(startDate, today);
                if (toEarly) System.out.println("\tErreur - Date de début (" + startDate
                        + ") doit être >= date d'aujourd'hui ("+ today + ")");
            } while (toEarly);
            theService.setStartDate(startDate);

            String endDate;
            do {
                endDate = readField(scanner, "Date de fin (JJ-MM-AAAA)", CommonRegEx.VALID_DATE);
                toEarly = isBefore(endDate, startDate);
                if (toEarly) System.out.println("\tErreur - Date de fin (" + endDate
                        + ") doit être après date de début ("+ startDate + ")");
            } while (toEarly);
            theService.setEndDate(endDate);

            String time = readField(scanner, "Heure du service", CommonRegEx.VALID_TIME);
            theService.setTime(time);

            Boolean[] occurrence = new Boolean[7];
            for (int i=0; i<occurrence.length; i++) {
                occurrence[i] = confirm(scanner, "Le service aura lieu les " + Service.WEEK_DAY_LONG[i]);
            }
            theService.setOccurrence(occurrence);

            int maxParticipants = readIntField(scanner, "Nombre maximum de participants", 1, 1000);
            theService.setMaxParticipants(maxParticipants);

            int professionalId;
            Boolean notValid;
            do {
                professionalId = readIntField(scanner, "Numéro du professionnel", 1, Professional.getLastId());
                Professional professional = professionalRepository.find(professionalId);
                if (professional == null || professional.getDeleted()) {
                    notValid = true;
                    System.out.println("\tNuméro de professionnel invalide : " + professionalId);
                } else if (professional.getSuspended()) {
                    notValid = true;
                    System.out.println("\tProfessionnel supendu!!!");
                } else notValid = false;
            } while (notValid);
            theService.setProfessionalId(professionalId);

            int cost = readIntField(scanner, "Frais du service", 0, Service.MAX_PRICE);
            theService.setCost(cost);

            String comment = readField(scanner, "Comentaire", CommonRegEx.VALID_COMMENT);
            theService.setComment(comment);

            serviceRepository.add(theService);   // Create member in the repository
            System.out.println("\n\nLe service suivant a été créé : \n" + theService.details());
        }

        // Update existing member
        else if (theService != null && !deleteService) {
            // TODO - this is not optimal, but it will do for the prototype... (needs generalization)
            System.out.println("\t*** Modification d'un service existant ***");

            String name = updateField(scanner, "Nom du service", theService.getName(),
                    CommonRegEx.VALID_NAME);
            if (!name.isEmpty()) theService.setName(name);

            boolean toEarly;
            String today = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
            String startDate;
            do {
                startDate = updateField(scanner, "Date de début (JJ-MM-AAAA)", theService.getStartDate(),
                        CommonRegEx.VALID_DATE);
                if (startDate.isEmpty()) break;
                toEarly = isBefore(startDate, today);
                if (toEarly) System.out.println("\tErreur - Date de début (" + startDate
                        + ") doit être >= date d'aujourd'hui ("+ today + ")");
            } while (toEarly);
            if (!startDate.isEmpty()) theService.setStartDate(startDate);

            String endDate;
            do {
                endDate = updateField(scanner, "Date de fin (JJ-MM-AAAA)", theService.getEndDate(),
                        CommonRegEx.VALID_DATE);
                if (endDate.isEmpty()) break;
                toEarly = isBefore(endDate, startDate);
                if (toEarly) System.out.println("\tErreur - Date de fin (" + endDate
                        + ") doit être après date de début ("+ startDate + ")");
            } while (toEarly);
            if (!endDate.isEmpty()) theService.setEndDate(endDate);

            String time = updateField(scanner, "Heure du service", theService.getTime(),
                    CommonRegEx.VALID_TIME);
            theService.setTime(time);

            Boolean[] occurrence = new Boolean[7];
            for (int i=0; i<occurrence.length; i++) {
                System.out.println("\tOccurence actuelle pour les '" + Service.WEEK_DAY_LONG[i] + "' : "
                        + (occurrence[i] ? "oui" : "non"));
                occurrence[i] = confirm(scanner, "Le service aura lieu les " + Service.WEEK_DAY_LONG[i]);
            }
            theService.setOccurrence(occurrence);

            int maxParticipants = updateIntField(scanner, "Nombre maximum de participants", theService.getMaxParticipants(),
                    1, 1000);
            theService.setMaxParticipants(maxParticipants);

            String comment = updateField(scanner, "Comentaire", theService.getComment(),
                    CommonRegEx.VALID_COMMENT);
            if (!comment.isEmpty()) theService.setComment(comment);

            serviceRepository.add(theService);   // Update service in the repository
            System.out.println("\n\nLe service suivant a été modifié : \n" + theService.details());
        }

    }


    // *** Utility Methods **************************************

    // Utility method that asks user to input value for a field and validate the value before returning it
    private static String updateField(Scanner scanner, String fieldName, String fieldValue, Pattern pattern) {
        String answer;
        Boolean isNotValid;
        do {
            System.out.println("\tValeur actuelle pour le champ '" + fieldName + "' : " + fieldValue);
            System.out.print("\tEntrez la nouvelle valeur (ou 0 pour ne pas modifier) :> ");
            answer = scanner.nextLine();
            if (answer.equals("0")) {
                answer = "";
                isNotValid = false;
            } else {
                isNotValid = !isValidField(pattern, answer);
                if (isNotValid) {
                    System.out.println("\tErreur - '" + fieldName + "' invalide : " + answer);
                } else {
                    System.out.println("\t" + fieldName + " : " + answer);
                    isNotValid = !confirm(scanner, "\tConfirmez l'entrée");
                }
            }
        } while (isNotValid);   // ask until a valid value is entered
        return answer;
    }

    // Utility method that asks user to input value for a field and validate the value before returning it
    private static String readField(Scanner scanner, String fieldName, Pattern pattern) {
        String answer;
        Boolean isNotValid;
        do {
            System.out.print("\tEntrez la valeur pour le champ '" + fieldName + "' :> ");
            answer = scanner.nextLine();
            isNotValid = !isValidField(pattern, answer);
            if (isNotValid) {
                System.out.println("\tErreur - '" + fieldName + "' invalide : " + answer);
            } else {
                System.out.println("\t" + fieldName + " : " + answer);
                isNotValid = !confirm(scanner, "\tConfirmez l'entrée");
            }
        } while (isNotValid);   // ask until a valid value is entered
        return answer;
    }

    // Utility method that asks user to input value for a field and validate the value before returning it
    private static int updateIntField(Scanner scanner, String fieldName, int fieldValue, int minVal, int maxVal) {
        int value = 0;
        boolean isNotValid;
        do {
            System.out.println("\tValeur actuelle pour le champ '" + fieldName + "' : " + fieldValue);
            System.out.print("\tEntrez la nouvelle valeur (ou 0 pour ne pas modifier) :> ");
            if (scanner.hasNextInt()) {
                value = scanner.nextInt();
                if (value == 0) {
                    isNotValid = false;
                } else {
                    isNotValid = value < minVal || value > maxVal;
                    if (isNotValid) {
                        System.out.println("\tErreur - '" + fieldName + "' invalide : " + value);
                    } else {
                        System.out.println("\t" + fieldName + " : " + value);
                        isNotValid = !confirm(scanner, "\tConfirmez l'entrée");
                    }
                }
            } else {
                String answer = scanner.nextLine();
                System.out.println("\tErreur - '" + fieldName + "' invalide : " + answer);
                isNotValid = true;
            }
        } while (isNotValid);   // ask until a valid value is entered
        return value;
    }


    // Utility method that asks user to input value for an int field and validate the value before returning it
    private static int readIntField(Scanner scanner, String fieldName, int minVal, int maxVal) {
        Boolean isNotValid;
        int value = 0;
        do {
            System.out.print("\tEntrez la valeur pour le champ '" + fieldName + "' :> ");
            if (scanner.hasNextInt()) {
                value = scanner.nextInt();
                isNotValid = value < minVal || value > maxVal;
                if (isNotValid) {
                    System.out.println("\tErreur - '" + fieldName + "' invalide : " + value);
                } else {
                    System.out.println("\t" + fieldName + " : " + value);
                    isNotValid = !confirm(scanner, "\tConfirmez l'entrée");
                }
            } else {
                String answer = scanner.nextLine();
                System.out.println("\tErreur - '" + fieldName + "' invalide : " + answer);
                isNotValid = true;
            }
        } while (isNotValid);
        return value;
    }

    // Utility method that print a message and ask for comfirmation
    private static Boolean confirm(Scanner scanner, String message) {
        String answer;
        Boolean choice = false;
        do {
            System.out.print(message + " (o/n)? ");
            answer = scanner.nextLine();
            if (answer.matches("^[oO]$")) {
                choice = true;
            } else if (!answer.matches("^[nN]$")) {
                answer = ""; // wrong answer, ask again the same question
            }
        } while (answer.isEmpty());

        return choice;
    }

    // Utility method that tells if a field matches a pattern
    private static Boolean isValidField(Pattern pattern, String field) {
        Matcher matcher = pattern.matcher(field);
        return matcher.find();
    }

    // Utility methods that compare to string date in format dd-MM-yyyy
    // returns true if the first one is before the second one
    private static Boolean isBefore(String before, String after) {
        // TODO - bug, cannot parse date in this format!???! Default to false for now
        LocalDate d1 = LocalDate.parse(before, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        LocalDate d2 = LocalDate.parse(after, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        return d1.isBefore(d2);
    }
}
