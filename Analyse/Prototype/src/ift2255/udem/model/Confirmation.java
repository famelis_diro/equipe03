package ift2255.udem.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

// Confirmation class is a record keeping track of confirmation of member registration to service
public class Confirmation {
    private String creationDate;              // (DD-MM-yyyy HH:MM:SS)
    private int professionalId;               // (9 digits)
    private int memberId;                     // (9 digits)
    private int serviceId;                    // (7 digits)
    private String comment;                   // (100 characters) (optional)


    public Confirmation() {
        this.creationDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
        this.professionalId = 0;
        this.memberId = 0;
        this.serviceId = 0;
        this.comment = "";
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public int getProfessionalId() {
        return professionalId;
    }

    public void setProfessionalId(int professionalId) {
        this.professionalId = professionalId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
