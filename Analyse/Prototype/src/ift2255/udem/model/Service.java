package ift2255.udem.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

// A Service is a record that holds all information related to a service
public class Service {
    public static final int MAX_PRICE = 100; // maximum cost for a service

    private static final int MAX_ID = 9999999;
    private static int lastId = 1;
    private static final Character[] WEEK_DAY = {'D', 'L', 'M', 'M', 'J', 'V', 'S'};
    public static final String[] WEEK_DAY_LONG =
            {"Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"};


    private int id;                           // (7 digits)
    private String creationDate;              // (DD-MM-yyyy HH:MM:SS)
    private String modificationDate;          // (DD-MM-yyyy HH:MM:SS)
    private String name;                      // name du service
    private String startDate;                 // (DD-MM-yyyy)
    private String endDate;                   // (DD-MM-yyyy)
    private String time;                      // (HH:MM)
    private Boolean[] occurrence;             // weekly occurrence
    private Boolean deleted;                  // true if service is scheduled for deletion
    private int maxParticipants;              // maximum participants allowed
    private int cost;                         // (in $) (max 100$)
    private int professionalId;               // (9 digits)
    private String comment;                   // (100 characters) (optional)

    // TODO - this hash map shouldn't be there, since participants are not for a service
    // TODO   but for a "seance" of a service... but let it be for now...
    // TODO   (need to have an entity seance, probably...)
    private HashMap<Integer, Member> participants; // the list of participants ordered by their member number


    // create a new user with empty attributes except for id and creation date
    public Service() throws IdOutOfRangeException {
        this.id = nextId();
        this.creationDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
        this.modificationDate = creationDate;
        this.name = "";
        this.startDate = "";
        this.endDate = "";
        this.time = "";
        // occurrence has 7 boolean value indicating if the service occurs on the corresponding day of the week
        this.occurrence = new Boolean[]{false, false, false, false, false, false, false};
        this.deleted = false;
        this.maxParticipants = 0;
        this.cost = 0;
        this.professionalId = 0;
        this.comment = "";

        this.participants = new HashMap<>();
    }

    private int nextId() throws IdOutOfRangeException {
        if (lastId > MAX_ID) throw new IdOutOfRangeException();
        return lastId++;
    }

    public static Boolean idIsValid(int id) {
        return id > 0 && id <= lastId;
    }

    public int getId() {
        return id;
    }

    public static int getLastId() {
        return lastId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    private void setModificationDate() {
        this.modificationDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        setModificationDate();
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
        setModificationDate();
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
        setModificationDate();
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
        setModificationDate();
    }

    // return true if end date of the service is in the future (compare to today date)
    public boolean isInTheFuture() {
        LocalDateTime now = LocalDateTime.now();
        LocalDate localEndDate = LocalDate.parse(endDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        LocalTime localTime = LocalTime.parse(time, DateTimeFormatter.ofPattern("HH:mm"));
        LocalDateTime fullEndDate = LocalDateTime.of(localEndDate, localTime);

        return now.isBefore(fullEndDate);
    }

    // return the 10 next possible dates for an event from the current date
    public ArrayList<String> possibleDates() {
        // TODO - it only returns the 10 first date to simplify choice, extends to successive calls...
        LocalDateTime now = LocalDateTime.now();
        LocalDate localEndDate = LocalDate.parse(endDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        LocalTime localTime = LocalTime.parse(time, DateTimeFormatter.ofPattern("HH:mm"));
        LocalDateTime fullEndDate = LocalDateTime.of(localEndDate, localTime);

        ArrayList<String> dates = new ArrayList<>();
        if (fullEndDate.isBefore(now)) return dates;

        now = now.withHour(Integer.parseInt(time.substring(0,2))).
                withMinute(Integer.parseInt(time.substring(3,5)));
        int dayOfWeek = now.getDayOfWeek().getValue() - 1;
        int count = 0;
        while (now.isBefore(fullEndDate) && count < 10) {
            if (occurrence[dayOfWeek]) {
                count++;
                dates.add(now.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
            }
            now = now.plusDays(1);
            dayOfWeek++;
            if (dayOfWeek > 6) dayOfWeek = 0;
        }

        return dates;
    }

    public Boolean[] getOccurrence() {
        return occurrence;
    }

    public void setOccurrence(Boolean[] occurrence) {
        this.occurrence = occurrence;
        setModificationDate();
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
        setModificationDate();
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
        setModificationDate();
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getProfessionalId() {
        return professionalId;
    }

    public void setProfessionalId(int professionalId) {
        this.professionalId = professionalId;
        setModificationDate();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
        setModificationDate();
    }

    public HashMap<Integer, Member> getParticipants() {
        return participants;
    }

    // add a member to this service, return null if it fails
    public Member add(Member member) {
        if (participants.size() > maxParticipants) return null;
        return participants.put(member.getId(), member);
    }

    // return the member with memberId if it is registered to this service, null otherwise
    public Member find(int memberId) {
        return participants.get(memberId);
    }

    // remove the member from this service and returns it, return null if member is not registered for this service
    public Member delete(int memberId) {
        return participants.remove(memberId);
    }

    // return the number of participants so far
    public int getNbParticipants() {
        return participants.size();
    }

    // return true if the service is full
    public boolean isFull() {
        return participants.size() >= maxParticipants;
    }


    @Override
    public String toString() {
        return id + "\n" +
                creationDate + "\n" +
                modificationDate + "\n" +
                name + "\n" +
                startDate + "\n" +
                endDate + "\n" +
                time + "\n" +
                occurrence + "\n" +
                deleted + "\n" +
                maxParticipants + "\n" +
                cost + "\n" +
                professionalId + "\n" +
                comment + "\n";
    }

    // print the details of the service in long format
    public String details() {
        StringBuilder result = new StringBuilder("Code de service : " + id + "\n" +
                "Date de création : " + creationDate + "\n" +
                "Date de modification : " + modificationDate + "\n" +
                "Nom du service : " + name + "\n" +
                "Date de début : " + startDate + "\n" +
                "Date de fin : " + endDate + "\n" +
                "Heure : " + time + "\n" +
                "Occurrence hebdomadaire : ");
        for (int i=0; i<occurrence.length; i++) {
            result.append(occurrence[i] ? WEEK_DAY[i] : "_");
        }

        result.append("\n")
                .append("Supprimé : ").append(deleted ? "oui" : "non").append("\n")
                .append("Nombre actuel de participant : ").append(getNbParticipants()).append("\n")
                .append("Nombre maximum de participant : ").append(maxParticipants).append("\n")
                .append("Frais du service : ").append(cost).append("\n")
                .append("Numéro du professinonel : ").append(professionalId).append("\n")
                .append("Commentaire : ").append(comment).append("\n");
        return result.toString();
    }

    // used to print a table header for the list of services (to be used with detailsShort)
    public static String detailsShortHeader() {
        return "    Code   Num. Prof     Début        Fin     Heure   Fréqu.  Nb part   $   Nom du service\n" +
               "  -------  ---------  ----------  ----------  -----  -------  -------  ---  --------------";
    }

    // print the details for a given service (to be used with detailsShortHeader)
    public String detailsShort() {
        StringBuilder occString = new StringBuilder();
        for (int i=0; i<occurrence.length; i++) {
            occString.append(occurrence[i] ? WEEK_DAY[i] : ".");
        }
        return String.format("  %7d  %9d  %s  %s  %s  %s  %3d/%3d  %3d  %s", id, professionalId,
                startDate, endDate, time, occString, getNbParticipants(), maxParticipants, cost, name);
    }
}
