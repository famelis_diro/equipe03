package ift2255.udem.model;

// Professional is a record holding all information of a user
public class Professional extends User {

    private static int lastId = 1;

    // create a new Professional with empty attributes except for professionalId and creation date
    public Professional() throws IdOutOfRangeException {
        super();
    }

    @Override
    protected int nextId() throws IdOutOfRangeException {
        if (lastId > MAX_ID) throw new IdOutOfRangeException();
        return lastId++;
    }

    public static int getLastId() {
        return lastId;
    }

    // a quick check of an id validity (it must be smaller than the last assigned id)
    public static Boolean idIsValid(int id) {
        return id > 0 && id <= lastId;
    }

}
