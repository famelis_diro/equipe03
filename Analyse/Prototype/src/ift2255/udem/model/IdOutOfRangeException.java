package ift2255.udem.model;

// Exception used in case we ran out of id since they are limited in the number of digits (7 or 9)
public class IdOutOfRangeException extends Exception {

    public IdOutOfRangeException() {
    }
}
