package ift2255.udem.model;

// Member is a record holding all information of a user
public class Member extends User {

    private static int lastId = 1;


    // create a new member with empty attributes except for memberId and creation date
    public Member() throws IdOutOfRangeException {
        super();
    }

    @Override
    protected int nextId() throws IdOutOfRangeException {
        if (lastId > MAX_ID) throw new IdOutOfRangeException();
        return lastId++;
    }

    // a quick check of an id validity (it must be smaller than the last assigned id)
    public static Boolean idIsValid(int id) {
        return id > 0 && id <= lastId;
    }

    public static int getLastId() {
        return lastId;
    }
}
