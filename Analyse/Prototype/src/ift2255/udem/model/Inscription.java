package ift2255.udem.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

// Inscription keeps tracks of member registration to a specific occurrence of a service
public class Inscription {
    private String creationDate; // (DD-MM-yyyy HH:MM:SS)
    private String serviceDate;  // (DD-MM-yyyy)
    private int professionalId;  // (9 digits)
    private int memberId;        // (9 digits)
    private int serviceId;       // (7 digits)
    private String comment;      // (100 characters) (optional)


    // create a new inscription with empty attributes except for creation date
    public Inscription() {
        this.creationDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
        this.serviceDate = "";
        this.professionalId = 0;
        this.memberId = 0;
        this.serviceId = 0;
        this.comment = "";
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(String serviceDate) {
        this.serviceDate = serviceDate;
    }

    public int getProfessionalId() {
        return professionalId;
    }

    public void setProfessionalId(int professionalId) {
        this.professionalId = professionalId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    // used to print information about an inscription
    public String detailsShort() {
        return "Numéro du membre : " + memberId
                + ", Code de service : " + serviceId
                + ", Date de la séance : " + serviceDate;
    }
}
