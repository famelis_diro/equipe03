package ift2255.udem.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

// TODO - use generics to factorize with Member and Professional Repository

// ServiceRepository keeps track of all services in a hash map
// the hash map key is: serviceId
public class ServiceRepository {
    private static ServiceRepository _instance = null;

    // TODO - eventually replace with file... (but for prototype keep everything in memory)
    private HashMap<Integer, Service> serviceHashMap;

    private ServiceRepository() {
        serviceHashMap = new HashMap<>();

        populate(); // hack to populate the database (for prototype only)
    }

    public static ServiceRepository getInstance() {
        if (_instance == null) {
            _instance = new ServiceRepository();
        }
        return _instance;
    }

    // CRUD (Create, Read, Update and Delete) mocking implementations

    // CRUD's Create and Update
    public void add(Service service) {
        serviceHashMap.put(service.getId(), service);
    }

    // CRUD's Delete
    public void delete(int serviceId) {
        serviceHashMap.remove(serviceId);
    }


    // CRUD's Read
    public Service find(int serviceId) {
        return serviceHashMap.get(serviceId);
    }


    public Collection<Service> all() {
        return serviceHashMap.values();
    }

    public boolean isEmpty() {
        return serviceHashMap.isEmpty();
    }


    // a hack method to populate the database
    private void populate() {
        try {
            Service item = new Service();
            item.setName("Thérapie transcendantale");
            item.setProfessionalId(1);
            item.setMaxParticipants(1);
            item.setTime("23:45");
            item.setOccurrence(new Boolean[]{true,false,false,true,false,true,false});
            LocalDateTime now = LocalDateTime.now();
            item.setStartDate(now.format(DateTimeFormatter.ofPattern("dd-MM-yyy")));
            LocalDateTime later = now.plusDays(3);
            item.setEndDate(later.format(DateTimeFormatter.ofPattern("dd-MM-yyy")));
            item.setComment("Retrouver votre vrai moi pour une modique somme!");
            item.setCost(100);
            add(item);

            item = new Service();
            item.setName("Yoga Quantique");
            item.setProfessionalId(2);
            item.setMaxParticipants(10);
            item.setTime("00:15");
            item.setOccurrence(new Boolean[]{false,true,true,false,true,false,true});
            item.setStartDate(now.format(DateTimeFormatter.ofPattern("dd-MM-yyy")));
            later = now.plusDays(14);
            item.setEndDate(later.format(DateTimeFormatter.ofPattern("dd-MM-yyy")));
            item.setComment("Le yoga des états supperposés!");
            item.setCost(75);
            add(item);

            item = new Service();
            item.setName("Ping Pong Imaginaire");
            item.setProfessionalId(1);
            item.setMaxParticipants(7);
            item.setTime("02:05");
            item.setOccurrence(new Boolean[]{true,true,true,true,true,true,true});
            item.setStartDate(now.format(DateTimeFormatter.ofPattern("dd-MM-yyy")));
            later = now.plusDays(5);
            item.setEndDate(later.format(DateTimeFormatter.ofPattern("dd-MM-yyy")));
            item.setComment("Si je ping, tu pong et si je pong tu ping. Think about that!");
            item.setCost(99);
            add(item);
        } catch (IdOutOfRangeException e) {
            // do nothing
        }
    }
}
