# IFT 2255 | A 2018

## Benkirane, Desmarais et Le Bourdais-Gross, experts analystes conseils

Ce répertoire contient les répertoires utiliser dans la réalisation des TP01 et TP02.

* Le répertoire [Analyse](https://bitbucket.org/famelis_diro/equipe03/src/master/Analyse/) contient les fichiers utiliser pour la réalisation du TP02.

* Le répertoire [Exigences/CU](https://bitbucket.org/famelis_diro/equipe03/src/master/Exigences/CU/) contient les fichiers utilisés pour la réalisation du TP01 ainsi que la mise jour requise par le TP02.

* Le répertoire [Implementation](https://bitbucket.org/famelis_diro/equipe03/src/master/Implementation/) contient l'exécutable et les sources du prototype pour le TP02.

